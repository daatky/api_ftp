'use strict';
var express = require('express');
var fs = require('fs');

//const url_server = 'http://localhost:4000';
const url_server = 'https://newspoth.com';

class SpotControlador {


    /**
    * Upload file file to multimedia/plantillas
    * @param  {file, tipo, id_plantilla}
    * tipo: {
    *   0: banner_pc,
    *   1: fondo_pc,
    *   2: icono_1,
    *   3: icono_2,
    *   4: icono_3,
    *   5: icono_4,
    * }
    * @return {data}
    */
    uploadFileMaqueta(req,res){
        let data;
        if(req.files['file'] && req.body.id_maqueta && req.body.tipo) {
            if(req.files['file'][0].mimetype=='image/jpg' || req.files['file'][0].mimetype=='image/jpeg') {
                var tmp_path = req.files['file'][0].path;
                var target_path = getPathFile(req.body.id_maqueta, parseInt(req.body.tipo));
                console.log(target_path);
                coyFile(tmp_path, target_path, function(estado){
                    if(estado) {
                        data = {code: 0, data: 'Archivo subido correctamente', url: url_server + '/' + target_path.split('/')[1] + '/' + target_path.split('/')[2]};
                        res.json(data);
                    }else{
                        data = {code: -1, data: 'Ocurrió un error al subir el archivo'};
                        res.json(data);
                    }
                });
            }else{
                data = {code: -2, data: 'Error, el formato de imagen debe ser .jpeg'};
                res.json(data);
            }
        }else{
            data = {code: -1, data: 'Error, existen campos incompletos'};
            res.json(data);
        }
    }

    uploadFileMaquetaModuloContenido(req,res){
        let data;
        if(req.files['file'] && req.body.path) {
            var tmp_path = req.files['file'][0].path;
            var target_path = 'multimedia/' + req.body.path;
            console.log(target_path);
            coyFile(tmp_path, target_path, function(estado){
                if(estado) {
                    data = {code: 0, data: 'Archivo subido correctamente', url: url_server + '/' + req.body.path};
                    res.json(data);
                }else{
                    data = {code: -1, data: 'Ocurrió un error al subir el archivo'};
                    res.json(data);
                }
            });
        }else{
            data = {code: -1, data: 'Error, existen campos incompletos'};
            res.json(data);
        }
    }

    /**
    * Upload file anuncio to multimedia/anuncios
    * @param  {path, id_pub}
    * @return {data}
    */
    uploadFileAnuncio(req,res){
        let data;
        if(req.files['file'] && req.body.path) {
            var tmp_path = req.files['file'][0].path;
            var target_path = 'multimedia/' + req.body.path;
            console.log(target_path);
            coyFile(tmp_path, target_path, function(estado){
                if(estado) {
                    data = {code: 0, data: 'Archivo subido correctamente', url: url_server + '/' + req.body.path};
                    res.json(data);
                }else{
                    data = {code: -1, data: 'Ocurrió un error al subir el archivo'};
                    res.json(data);
                }
            });
        }else{
            data = {code: -1, data: 'Error, existen campos incompletos'};
            res.json(data);
        }
    }
}

module.exports = SpotControlador;

function coyFile(tmp_path, target_path, callback){
    var src = fs.createReadStream(tmp_path);
    var dest = fs.createWriteStream(target_path);
    src.pipe(dest);
    src.on('end', function(){
       callback(true);
    });
    src.on('error', function(err) { 
        callback(false);
    }); 
}

function getPathFile(id_maqueta, tipo){
    if(tipo === 0) { //banner_pc
        return 'multimedia/plantillas/banner_pc_'+id_maqueta+'.jpg';
    } else if(tipo === 1) { //fondo_pc
        return 'multimedia/plantillas/fondo_pc_'+id_maqueta+'.jpg';
    } else if(tipo === 2) { //banner_tab
        return 'multimedia/plantillas/banner_tab_'+id_maqueta+'.jpg';
    } else if(tipo === 3) { //fondo_tab
        return 'multimedia/plantillas/fondo_tab_'+id_maqueta+'.jpg';
    } else if(tipo === 4) { //banner_mov
        return 'multimedia/plantillas/banner_mov_'+id_maqueta+'.jpg';
    } else if(tipo === 5) { //fondo_tab
        return 'multimedia/plantillas/fondo_mov_'+id_maqueta+'.jpg';
    } else if(tipo === 6) { //icono_a
        return 'multimedia/plantillas/icono_a_'+id_maqueta+'.jpg';
    } else if(tipo === 7) { //icono_b
        return 'multimedia/plantillas/icono_b_'+id_maqueta+'.jpg';
    } else if(tipo === 8) { //icono_c
        return 'multimedia/plantillas/icono_c_'+id_maqueta+'.jpg';
    } else if(tipo === 9) { //icono_d
        return 'multimedia/plantillas/icono_d_'+id_maqueta+'.jpg';
    }
}

function getPathFilePc(id_plantilla, tipo){
    if(tipo == 0) {
        return 'multimedia/plantillas/bannerPc_'+id_plantilla+'.png';
    }else if(tipo == 1){
        return 'multimedia/plantillas/fondoPc_'+id_plantilla+'.png';
    }else if(tipo == 2){
        return 'multimedia/plantillas/iconoPc1'+id_plantilla+'.png';
    }else if(tipo == 3){
        return 'multimedia/plantillas/iconoPc2'+id_plantilla+'.png';
    }else if(tipo == 4){
        return 'multimedia/plantillas/iconoPc3'+id_plantilla+'.png';
    }else if(tipo == 5){
        return 'multimedia/plantillas/iconoPc4'+id_plantilla+'.png';
    }
}

function getPathFileMovilPortrait(id_plantilla, tipo){
    if(tipo == 0) {
        return 'multimedia/plantillas/bannerMovilPortrait_'+id_plantilla+'.png';
    }else if(tipo == 1){
        return 'multimedia/plantillas/fondoMovilPortrait_'+id_plantilla+'.png';
    }else if(tipo == 2){
        return 'multimedia/plantillas/iconoMovilPortrait1_'+id_plantilla+'.png';
    }else if(tipo == 3){
        return 'multimedia/plantillas/iconoMovilPortrait2_'+id_plantilla+'.png';
    }else if(tipo == 4){
        return 'multimedia/plantillas/iconoMovilPortrait3_'+id_plantilla+'.png';
    }else if(tipo == 5){
        return 'multimedia/plantillas/iconoMovilPortrait4_'+id_plantilla+'.png';
    }
}

function getPathFileMovilLandScape(id_plantilla, tipo){
    if(tipo == 0) {
        return 'multimedia/plantillas/bannerMovilLandScape_'+id_plantilla+'.png';
    }else if(tipo == 1){
        return 'multimedia/plantillas/fondoMovilLandScape_'+id_plantilla+'.png';
    }else if(tipo == 2){
        return 'multimedia/plantillas/iconoMovilLandScape1_'+id_plantilla+'.png';
    }else if(tipo == 3){
        return 'multimedia/plantillas/iconoMovilLandScape2_'+id_plantilla+'.png';
    }else if(tipo == 4){
        return 'multimedia/plantillas/iconoMovilLandScape3_'+id_plantilla+'.png';
    }else if(tipo == 5){
        return 'multimedia/plantillas/iconoMovilLandScape4_'+id_plantilla+'.png';
    }
}

function getPathFileTabletPortrait(id_plantilla, tipo){
    if(tipo == 0) {
        return 'multimedia/plantillas/bannerTabletPortrait_'+id_plantilla+'.png';
    }else if(tipo == 1){
        return 'multimedia/plantillas/fondoTabletPortrait_'+id_plantilla+'.png';
    }else if(tipo == 2){
        return 'multimedia/plantillas/iconoTabletPortrait1_'+id_plantilla+'.png';
    }else if(tipo == 3){
        return 'multimedia/plantillas/iconoTabletPortrait2_'+id_plantilla+'.png';
    }else if(tipo == 4){
        return 'multimedia/plantillas/iconoTabletPortrait3_'+id_plantilla+'.png';
    }else if(tipo == 5){
        return 'multimedia/plantillas/iconoTabletPortrait4_'+id_plantilla+'.png';
    }
}

function getPathFileTabletLandScape(id_plantilla, tipo){
    if(tipo == 0) {
        return 'multimedia/plantillas/bannerTabletLandScape_'+id_plantilla+'.png';
    }else if(tipo == 1){
        return 'multimedia/plantillas/fondoTabletLandScape_'+id_plantilla+'.png';
    }else if(tipo == 2){
        return 'multimedia/plantillas/iconoTabletLandScape1_'+id_plantilla+'.png';
    }else if(tipo == 3){
        return 'multimedia/plantillas/iconoTabletLandScape2_'+id_plantilla+'.png';
    }else if(tipo == 4){
        return 'multimedia/plantillas/iconoTabletLandScape3_'+id_plantilla+'.png';
    }else if(tipo == 5){
        return 'multimedia/plantillas/iconoTabletLandScape4_'+id_plantilla+'.png';
    }
}
