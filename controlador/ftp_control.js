'use strict';
const express = require('express');
const fs = require('fs');
const ftp = require('ftp');

class FtpControlador {

    /**
     * Actualiza el archivo login.html de cada equipo dentro de la carpeta hotspot/login.html
     * El archivo que reemplaza no es el archivo por defecto, si no el que redirecciona al newspoth
     * @param  {contenido}
     * @return {html}
     */
    changeLoginFileOnly(req,res) {
        console.log(req.body.data);
        let equipo = JSON.parse(req.body.data); 
        console.log(equipo);
        let c = new ftp();
        c.on('error', (err) => {
            res.json({code:-1, mensaje:'Error al tratar de conectarse al equipo', error: err});
        });
        c.on('ready', function () {
            fs.writeFile("ftp_files/hotspot/login.html", getFileContenido(equipo.codigo, equipo.url_red), (err) => {
                if (err) {
                    res.json({code:-1, mensaje:'Error al sobreescribir el archivo login.html en el servidor local', error: err});
                } else {
                    console.log("Successfully Written to File.");
                    c.put('ftp_files/hotspot/login.html', 'hotspot/login.html', function (err) {
                        if (err) {
                            res.json({code:-1, mensaje:'Error al tratar de subir el archivo login.html al hotspot', error: err});
                            c.end();
                        } else {
                            c.end();
                            res.json({code:0, mensaje:'Archivo login actualizado con éxito'});
                        }
                    });
                }
            });
        });
        c.connect({
            host: equipo.equipo_ip,
            user: equipo.equipo_user,
            password: equipo.equipo_password,
            debug: console.log.bind(console)
        });
    }

    /**
     * Reestablecer la carpeta del hotspot a la carpeta por defecto utilizada anteriormente
     * @param  {equipos}
     * @return {html}
     */
    restoreDefaultHotspotFiles(req,res) {
        let equipo = JSON.parse(req.body.data);
        let c = new ftp();
        c.on('error', (err) => {
            res.json({code:-1, mensaje:'Error al tratar de conectarse al equipo', error: err});
        });
        c.on('ready', function () {
            c.rmdir('hotspot', true, function(err){
                if(err){
                    res.json({code:-1, mensaje:'Error al borrar la carpeta de archivos hotspot en el equipo', error: err});
                }else{
                    console.log('error');
                    let file_routes = getFileListOfDirectory();
                    console.log(file_routes);
                    c.mkdir('hotspot', true, function(err){
                        if(err){
                            res.json({code:-1, mensaje:'Error al crear el directorio principal en el equipo', error: err});
                        }else{
                            createDirectoriosRecursivo(c, 0, file_routes, function(status){
                                if(status) {
                                    console.log('Drectorios creados');
                                    //Directorios creados
                                    console.log('Copia de archivos en directorios del hotspot');
                                    putFilesRecursivo(c, pos, file_routes, function(status_b){
                                        if(status_b) {
                                            c.end();
                                            res.json({code:0, mensaje:'Carpeta de archivos hotspot restaurada en el equipo', error: err});
                                        } else {
                                            c.end();
                                            res.json({code:-1, mensaje:'Error al restaurar los directorios principales del hotspot por default en el equipo', error: err});
                                        }
                                    });
                                }else{
                                    c.end();
                                    res.json({code:0, mensaje:'Error al crear los directorios principales del hotspot por default en el equipo', error: err});
                                }
                            });
                           
                        }
                    });
                }
            });
        });
        c.connect({
            host: equipo.equipo_ip,
            user: equipo.equipo_user,
            password: equipo.equipo_password,
            debug: console.log.bind(console)
        });
    }

}

module.exports = FtpControlador;


/**
 * Genera el archivo login.html para 
 * @param  {contenido}
 * @return {html}
 */
function getFileContenido(codigo, url_red) {
    let html = '<html>';
    html += '<head>';
    html += '<title>SPOT</title>';
    html += '</head>';
    html += '<body>';
    html += '$(if chap-id)';
    html += '<noscript>';
    html += '<center><b>JavaScript required. Enable JavaScript to continue.</b></center>';
    html += '</noscript>';
    html += '$(endif) ';
    html += '<center>Si no eres redireccionado, click en continuar</center>';
    html += '<form name="redirect" action="'+url_red+'" method="POST">';
    html += '<input type="hidden" name="mac" value="$(mac)">';
    html += '<input type="hidden" name="ip" value="$(ip)">';
    html += '<input type="hidden" name="username" value="$(username)">';
    html += '<input type="hidden" name="link-login" value="$(link-login)">';
    html += '<input type="hidden" name="link-orig" value="$(link-orig)">';
    html += '<input type="hidden" name="error" value="$(error)">';
    html += '<input type="hidden" name="trial" value="$(trial)">';
    html += '<input type="hidden" name="chap-id" value="$(chap-id)">';
    html += '<input type="hidden" name="chap-challenge" value="$(chap-challenge)">';
    html += '<input type="hidden" name="linkloginonly" value="$(link-login-only)">';
    html += '<input type="hidden" name="linkorigesc" value="$(link-orig-esc)">';
    html += '<input type="hidden" name="macesc" value="$(mac-esc)">';
    html += '<input type="hidden" name="identity" value="$(identity)">';
    html += '<input type="hidden" name="bytes-in-nice" value="$(bytes-in-nice)">';
    html += '<input type="hidden" name="bytes-out-nice" value="$(bytes-out-nice)">';
    html += '<input type="hidden" name="session-time-left" value="$(session-time-left)">';
    html += '<input type="hidden" name="uptime" value="$(uptime)">';
    html += '<input type="hidden" name="refresh-timeout" value="$(refresh-timeout)">';
    html += '<input type="hidden" name="link-status" value="$(link-status)">';
    html += '<input type="hidden" name="codigo" value="' + codigo + '">';
    html += '<input type="submit" value="Continuar">';
    html += '</form>';
    html += '<script language="JavaScript">';
    html += 'document.redirect.submit();';
    html += '</script>        ';
    html += '</body>';
    html += '</html>';
    return html;
}


function getFileListOfDirectory(){
    let file_routes = [];
    fs.readdirSync('ftp_files/default/a15/hotspot').forEach(file => {
        console.log(file);
        let aux = {
            tipo: -1, //1 archivo, 0 directorio
            origen_path: 'ftp_files/default/a15/hotspot',
            main_path: '',
            file_name: '',
            sub_files: [],
        };

        if(file.toString().split('').indexOf('.') >= 0) {
            //Tipo: archivo
            aux.tipo = 1;
            aux.main_path = 'hotspot/';
            aux.file_name = file;
        }else{
            //Tipo: directorio
            aux.tipo = 0;
            aux.main_path = 'hotspot/'+file;
            fs.readdirSync('ftp_files/default/a15/hotspot/'+file).forEach(subfile => {
                aux.sub_files.push({
                    tipo: 1,
                    origen_path: 'ftp_files/default/a15/hotspot/'+file,
                    main_path: 'hotspot/'+file,
                    file_name: subfile
                });
            });
        }
        file_routes.push(aux);
    });
    return file_routes;
}

function createDirectoriosRecursivo(c, pos, file_routes, callback){
    if(pos < file_routes.length) {
        console.log('en la funcion');
        let f = file_routes[pos];
        console.log(f);
        if(f.tipo == 0) {
            console.log('en la funcion 2');
            c.mkdir(f.main_path, true, function(err){
                if(err){
                    console.log('Error al crear el directorio');
                    console.log(err);
                    callback(false);
                }else{
                    console.log('en la funcion 3'); 
                    pos = pos + 1;
                    createDirectoriosRecursivo(c, pos, file_routes, callback);
                }
            });
        } else {
            pos = pos + 1;
            createDirectoriosRecursivo(c, pos, file_routes, callback);
        }
    } else {
        callback(true);
    }
}

function putFilesRecursivo(c, pos, file_routes, callback){
    if(pos < file_routes.length) {
        console.log('en la funcion de archivos');
        let f = file_routes[pos];
        console.log(f);
        if(f.tipo == 1) {
            console.log('en la funcion 2 de archivos');
            c.put(f.origen_path+'/'+f.file_name, f.main_path+'/'+f.file_name, function (err) {
                if (err) {
                    console.log(err);
                    callback(false);
                } else {
                    console.log('archivo subido');
                    //Llamada recursiva
                    pos = pos + 1;
                    putFilesRecursivo(c, pos, file_routes, callback);
                }
            });
        } else {
            f.sub_files.forEach(function(subf){
                c.put(subf.origen_path+'/'+subf.file_name, subf.main_path+'/'+subf.file_name, function (err) {
                    if (err) {
                        console.log(err);
                    } 
                }); 
            });
            pos = pos + 1;
            putFilesRecursivo(c, pos, file_routes, callback);
        }
    } else {
        callback(true);
    }
}
