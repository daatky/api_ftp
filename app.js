const express = require('express');
const firebase = require('firebase-admin');
const serviceAccount = require('./serviceaccount.json');
const app = express();
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const ftp = require('ftp');
const fs = require('fs');
var server = http.createServer(app);

var spot_routes = require('./routes/spot');
var ftp_routes = require('./routes/ftp');

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount),
    databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com"
});

//configuraciones app
app.use(bodyParser.urlencoded({ extended: false }))
	.use(bodyParser.json())
	.set('trus proxy', 1)
	.use(cors({ origin: true }))
	.use(express.static("public"))
	.use(express.static("ftp_files"))
	.use(express.static("multimedia"))
	.set("view engine","pug");

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', false);
    next();
});

app.use('/spot', spot_routes);
app.use('/ftp', ftp_routes);

router.get('/', function(req,res){
	res.json({data: 'Hola mundo 1'});
});

app.use(router);

server.listen(4000, function(){
    console.log('Conectado y corriendo en 4000');
});




