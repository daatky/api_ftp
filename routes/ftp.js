var express = require('express');
var router = express.Router();

var ftp_control = require('../controlador/ftp_control');
var ftp_obj = new ftp_control();

router.post('/changeLoginFileOnly', ftp_obj.changeLoginFileOnly);
router.post('/restoreDefaultHotspotFiles', ftp_obj.restoreDefaultHotspotFiles);



module.exports = router;