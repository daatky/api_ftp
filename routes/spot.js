var express = require('express');
var router = express.Router();
var multer  = require('multer');
var upload = multer({ dest: 'multimedia/tmp_uploads/' });

var spot_control = require('../controlador/spot_control');
var spot_obj = new spot_control();

router.post('/uploadFileMaqueta', upload.fields([{ name: 'file', maxCount: 1 }]), spot_obj.uploadFileMaqueta);
router.post('/uploadFileMaquetaModuloContenido', upload.fields([{ name: 'file', maxCount: 1 }]), spot_obj.uploadFileMaquetaModuloContenido);
router.post('/uploadFileAnuncio', upload.fields([{ name: 'file', maxCount: 1 }]), spot_obj.uploadFileAnuncio);





module.exports = router;